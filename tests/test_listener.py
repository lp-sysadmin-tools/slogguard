# Copyright (C) 2011-2016 Laurent Pelecq <lpelecq+slogguard@circoise.eu>

# This file is part of Slogguard.
#
# Slogguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import slogguard.processor
import unittest

class TestListener(unittest.TestCase):

    """Test syslog listener."""

    def test_messageDecoding(self):
        """Test message decoding."""
        source_timestamp = 'Feb 14 17:02:40'
        source_hostname = 'hostname'
        source_msg = 'test: message 22976'
         # Examples from RFC 3164
        for source_facility, source_severity, source_pri in [ (slogguard.processor.FACILITY_USER, 6, '14'),
                                                              (slogguard.processor.FACILITY_LOCAL4, 5, '165') ]:
            data = '<' + source_pri + '>' + source_timestamp + ' ' + source_hostname + ' ' + source_msg
            facility, severity, timestamp, hostname, msg = slogguard.processor.SyslogProcessor.decode_message(data)
            self.assertEqual(facility, source_facility)
            self.assertEqual(severity, source_severity)
            self.assertEqual(timestamp, source_timestamp)
            self.assertEqual(hostname, source_hostname)
            self.assertEqual(msg, source_msg)

if __name__ == "__main__":
    unittest.main()
