# Copyright (C) 2011-2016 Laurent Pelecq <lpelecq+slogguard@circoise.eu>

# This file is part of Slogguard.
#
# Slogguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import re
import shutil
import slogguard.conf
import tempfile
import unittest

conf_file_content = r"""
parameters {
  server {
    address = '127.0.0.1'
    port = 5140
    cache_dir= "/var/lib/slogguard/cache"
  }

  mail {
    delay = 10 # minutes
    host = "localhost"
    sender = "daemon@domain.net"
    recipients = [ "root@domain.net" "postmaster@domain.net" ]
  }
}

actions {
  blacklist {
     command = blacklist("$ip")
     session = "ssh-$ip"
     expire = 2 # seconds
     threshold=2
  }
  report_abuse {
     command = abuse("$ip", "$tag")
  }
}

rules {
  % {
     ip = "(?P<ip>[1-9][0-9]*(\\.[0-9]+){3})"
  }
  AUTH [
    ignore    "[a-zA-Z]+(?:\\(\\w+\\))?\\[[0-9]*\\]: session opened"
    blacklist "sshd\\[[0-9]*\\]: Failed password for illegal user \\w* from %{ip}"
  ]
  DAEMON [
    ignore       "ntpd\\[[0-9]*\\]: kernel time sync status change"
    report_abuse "named\\[[0-9]*\\]: client %{ip}#.* query \\(cache\\) (?P<tag>.*) denied"
  ]
}
"""

rules = [
    ('ignore', "[a-zA-Z]+(?:\\(\\w+\\))?\\[[0-9]*\\]: session opened"),
    ('blacklist', "sshd\\[[0-9]*\\]: Failed password for illegal user \\w* from (?P<ip>[1-9][0-9]*(\\.[0-9]+){3})"),
    ('ignore', "ntpd\\[[0-9]*\\]: kernel time sync status change"),
    ('report_abuse', "named\\[[0-9]*\\]: client (?P<ip>[1-9][0-9]*(\\.[0-9]+){3})#.* query \\(cache\\) (?P<tag>.*) denied")
]

command_file_content = """
def blacklist(ip):
    pass

def abuse(ip, tag):
    pass
"""


unknown_param_conf_file_content = r"""
parameters {
  server {
    address = '127.0.0.1'
    port = 9999
    cache_dir= "/var/lib/slogguard/cache"
  }

  mail {
    delay = 10 # minutes
    host = "localhost"
    sender = "daemon@domain.net"
    recipients = [ "root@domain.net" "postmaster@domain.net" ]
  }

  whatever {
    key = "value"
  }
}
"""

wrong_type_conf_file_content = """actions {
  blacklist {
     command = blacklist("$ip")
     session = "ssh-$ip"
     expire = 2 # seconds
     threshold=D
  }
}
"""

class TestConf(unittest.TestCase):

    """Test configuration file parser."""

    def setUp(self):
        self.working_dir = tempfile.mkdtemp(prefix='tmp_slogguard')

    def write_config_file(self, content):
        conf_file = os.path.join(self.working_dir, 'slogguard.conf')
        with open(conf_file, 'w') as fh:
            fh.write(content)
        return conf_file

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    def test_config_content(self):
        """Test if configuration is properly parsed."""
        conf_file = self.write_config_file(conf_file_content)
        commands = {}
        exec(command_file_content, globals(), commands)
        cfg = slogguard.conf.read_config_file(conf_file, commands)
        # Check existence and type of each entry
        params = cfg.parameters
        for root_key, sub_key, value in [ ('server', 'address', '127.0.0.1'),
                                          ('server', 'port', 5140),
                                          ('server', 'cache_dir', "/var/lib/slogguard/cache"),
                                          ('mail', 'delay', 10),
                                          ('mail', 'host', 'localhost'),
                                          ('mail', 'sender', "daemon@domain.net"),
                                          ('mail', 'recipients', [ "root@domain.net", "postmaster@domain.net" ])]:
            self.assertIn(root_key, params)
            sub_cfg = params[root_key]
            self.assertTrue(hasattr(sub_cfg, sub_key))
            self.assertEqual(getattr(sub_cfg, sub_key), value)
        # Check rules number and type
        self.assertIn('rules', cfg)
        auth_rules = cfg.rules[slogguard.conf.FACILITY_AUTH]
        self.assertTrue(len(auth_rules), 2)
        daemon_rules = cfg.rules[slogguard.conf.FACILITY_DAEMON]
        self.assertTrue(len(daemon_rules), 1)
        r = re.compile("")
        i = 0
        for rule_list in [ auth_rules, daemon_rules ]:
            for rule in rule_list:
                self.assertTrue(hasattr(rule, 'regexp'))
                self.assertTrue(isinstance(rule.regexp, type(r)))
                self.assertEqual(rule.pattern, rules[i][1])
                i += 1
        session_rule = auth_rules[1]
        for attr in ['action', 'regexp']:
            self.assertTrue(hasattr(session_rule, attr))
        action = session_rule.action
        for attr, value in [('session', 'ssh-$ip'), ('expire', 2), ('threshold', 2)]:
            self.assertTrue(hasattr(action, attr))
            self.assertEqual(getattr(action, attr), value)
        self.assertTrue(hasattr(session_rule, 'action'))
        self.assertEqual(action.command.func, commands['blacklist'])
        self.assertEqual(action.command.args, ["$ip"])

    def test_unknown_parameter(self):
        """Test error when config contains an unknown parameter."""
        conf_file = self.write_config_file(unknown_param_conf_file_content)
        commands = {}
        exec(command_file_content, globals(), commands)
        self.assertRaises(slogguard.conf.SyntaxError, slogguard.conf.read_config_file, conf_file, commands)

    def test_wrong_parameter_type(self):
        """Test error when a parameter has a wrong type."""
        conf_file = self.write_config_file(wrong_type_conf_file_content)
        commands = {}
        exec(command_file_content, globals(), commands)
        self.assertRaises(slogguard.conf.SyntaxError, slogguard.conf.read_config_file, conf_file, commands)


if __name__ == "__main__":
    unittest.main()
