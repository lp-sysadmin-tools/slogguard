# Copyright (C) 2011-2016 Laurent Pelecq <lpelecq+slogguard@circoise.eu>

# This file is part of Slogguard.
#
# Slogguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import re
import slogguard.conf
import slogguard.handler
import time
import unittest
import unittest.mock

from tests.helpers.asserts import *

class UserCommand:

    def __init__(self):
        self.data = []

    def __call__(self, ip):
        self.data.append(ip)

def invalid_command():
    raise Exception('something wrong happened')

def make_action(command, session=None, expire=0, threshold=0):
    return slogguard.conf.Action(command, session, expire, threshold)

def make_rule(pattern, action=None):
    return slogguard.conf.Rule(re.compile(pattern), pattern, action)

def make_command(func, *args):
    return slogguard.conf.Command(func, args)

def make_auth_rules():
    auth_command = UserCommand()
    auth_rules = [
        make_rule(r"[a-zA-Z]+(?:\(\w+\))?\[[0-9]*\]: session opened"),
        make_rule(r"sshd\[[0-9]*\]: Failed password for illegal user \w* from (?P<ip>[1-9][0-9]*(\.[0-9]+){3}) ",
                  make_action(make_command(auth_command, "$ip"), "ssh-$ip", 2, 3))
    ]
    return auth_rules, auth_command

def make_daemon_rules():
    daemon_command = UserCommand()
    daemon_rules = [
        make_rule(r"ntpd\[[0-9]*\]: kernel time sync status change"),
        make_rule(r"named\[[0-9]*\]: client (?P<ip>[1-9][0-9]*(\.[0-9]+){3})#.* query \(cache\) .* denied",
                  make_action(make_command(daemon_command, "$ip"))),
        make_rule(r"resolver priming query complete", make_action(make_command(invalid_command)))
    ]
    return daemon_rules, daemon_command


class RuleProcessor:

    def __init__(self, rules):
        self.rules = rules

    def match(self, line):
        for matcher in self.rules:
            if matcher(line):
                return True
        return False


class TestMatcher(unittest.TestCase):

    """Test rule matcher."""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_rules(self, conf_rules):
        rules = [ slogguard.handler.Matcher.make(x) for x in conf_rules ]
        return rules

    def test_basic_rules(self):
        """Test basic rules processing."""
        daemon_rules, daemon_command = make_daemon_rules()
        rules = self.make_rules(daemon_rules)
        proc = RuleProcessor(rules)
        unmatched = []
        input_lines = [ 'ntpd[2304]: kernel time sync status change 0001',
                        "named[4018]: client 192.168.0.64#60865: query (cache) 'www.example.net/A/IN' denied",
                        'smartctl: /var 35% free=9228 Mb',
                        "named[4018]: client 192.168.0.32#30211: query (cache) 'www.example.net/A/IN' denied" ]
        for line in input_lines:
            if not proc.match(line):
                unmatched.append(line)

        self.assertEqual(unmatched, input_lines[2:3])
        self.assertEqual(daemon_command.data, ['192.168.0.64', '192.168.0.32'])

    def test_rules_with_limit(self):
        """Test rules with limit."""
        auth_rules, auth_command = make_auth_rules()
        rules = self.make_rules(auth_rules)
        proc = RuleProcessor(rules)
        unmatched = []
        input_lines = [ 'login(pam_unix)[3709]: session opened for user root by (uid=0)',
                        'sshd[30547]: Failed password for illegal user guest from 192.168.0.64 port 34392 ssh2',
                        'sshd[30549]: Illegal user admin from 10.0.0.164',
                        'sshd[30549]: Failed password for illegal user guest from 192.168.0.64 port 33502 ssh2',
                        'login(pam_unix)[3737]: session opened for user root by (uid=0)',
                        'sshd[30550]: Failed password for illegal user test from 192.168.0.64 port 34302 ssh2' ]
        for line in input_lines:
            if not proc.match(line):
                unmatched.append(line)

        self.assertEqual(unmatched, input_lines[2:3])
        self.assertEqual(auth_command.data, ['192.168.0.64'])

    def test_rules_with_expiration(self):
        """Test rules with expiration."""
        auth_rules, auth_command = make_auth_rules()
        rules = self.make_rules(auth_rules)
        proc = RuleProcessor(rules)
        input_lines = [ 'login(pam_unix)[3709]: session opened for user root by (uid=0)',
                        'sshd[30547]: Failed password for illegal user guest from 192.168.0.64 port 34392 ssh2',
                        'sshd[30549]: Illegal user admin from 10.0.0.164',
                        'sshd[30549]: Failed password for illegal user guest from 192.168.0.64 port 33502 ssh2',
                        'login(pam_unix)[3737]: session opened for user root by (uid=0)',
                        'sshd[30550]: Failed password for illegal user test from 192.168.0.64 port 34302 ssh2' ]
        for line in input_lines:
            if not proc.match(line):
                time.sleep(2)

        self.assertEqual(auth_command.data, [])

    def test_error_in_commands(self):
        """Test error in commands."""
        daemon_rules, daemon_command = make_daemon_rules()
        rules = self.make_rules(daemon_rules)
        proc = RuleProcessor(rules)
        unmatched = []
        input_lines = [ 'ntpd[2304]: kernel time sync status change 0001',
                        'resolver priming query complete' ]
        with unittest.mock.patch('logging.Logger.error') as mock_error:
            for line in input_lines:
                proc.match(line)
            assert_called_once(mock_error)


if __name__ == "__main__":
    unittest.main()
