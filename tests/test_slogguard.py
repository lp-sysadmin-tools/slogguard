#! /usr/bin/python -B

# Copyright (C) 2011-2017 Laurent Pelecq <lpelecq+slogguard@circoise.eu>

# This file is part of Slogguard.
#
# Slogguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import contextlib
import logging
import os
import shutil
import signal
import slogguard
import socket
import string
import sys
import tempfile
import threading
import time
import unittest
import unittest.mock

from tests.helpers.asserts import *

DAEMON = 3
AUTH = 4
CLOCK = 9

# Syntax: Facility, Ignore, Tag, message
# Tag is what expected to be captured or None for unmatched lines
log_lines = [
    (AUTH,   True,  None,   'su[546]: session opened'),
    (AUTH,   False, None,   'sshd[689]: Failed password for illegal user root from 10.0.23.65'),
    (AUTH,   False, None,   'CRON[3502]: pam_unix(cron:session): session opened for user root by (uid=0)'),
    (AUTH,   False, None,   'CRON[3502]: pam_unix(cron:session): session closed for user root'),
    (DAEMON, True,  None,   'ntpd[8436]: kernel time sync status change'),
    (DAEMON, False, '74.125.126.86', "named[452]: client 74.125.126.86#62238: query (cache) 'example.net/DS/IN' denied"),
    (AUTH,   True,  None,   'su[546]: session opened'),
    (DAEMON, False, None,   'smartctl: /var 35% free=9204 Mb'),
    (DAEMON, False, None,   'Protocol major versions differ for 45.33.70.146 port 34084'),
    (AUTH,   False, None,   'sshd[689]: Failed password for illegal user root from 10.0.23.65'),
    (AUTH,   False, None,   'CRON[3958]: pam_unix(cron:session): session opened for user root by (uid=0)'),
    (DAEMON, False, '2001:db8::0:1:0:0:1', "named[452]: client 2001:db8::0:1:0:0:1#23208: query (cache) 'example.net/DS/IN' denied"),
    (CLOCK,  False, None,   'Switched to clocksource kvm-clock'),
    (DAEMON, True,  None,   'ntpd[8436]: kernel time sync status change'),
    (AUTH,   False, None,   'sshd[689]: Failed password for illegal user root from 10.0.23.65'),
    (AUTH,   False, None,   'CRON[3958]: pam_unix(cron:session): session closed for user root'),
    (AUTH,   True,  None,   'su[8741]: session opened'),
]

action_file_content = string.Template("""#! /usr/bin/env python

def blacklist(ip):
    filename = os.path.join("$working_dir", 'blacklist.txt')
    with open(filename, 'a') as fh:
        fh.write('%s\\n'%(ip))
""")

conf_file_content = string.Template("""
parameters {
  server {
    address = '127.0.0.1'
    port = $port
    cache_dir= "$cache_dir"
  }
}

actions {
  posponed_blacklist {
     command = blacklist("$$ip")
     session = "ssh-$$ip"
     expire = 10 # seconds
     threshold=3
  }
  direct_blacklist {
     command = blacklist("$$ip")
  }
  external_blacklist {
     command = exec("/fake/bin/blacklist")
  }
}

rules {
  % {
    ip = "(?P<ip>[0-9a-z-][0-9a-z-]*(?:[.:][0-9a-z-]*)+)"
  }

  AUTH [
    ignore "[a-zA-Z]+(?:\\(\\w+\\))?\\[[0-9]*\\]: session opened"
    posponed_blacklist "sshd\\[[0-9]*\\]: Failed password for illegal user \\w* from %{ip}$$"
  ]

  DAEMON [
    ignore "ntpd\\[[0-9]*\\]: kernel time sync status change"
    direct_blacklist "named\\[[0-9]*\\]: client %{ip}#.* query \\(cache\\) .* denied"
    external_blacklist "Protocol major versions differ for %{ip} port"
  ]
}
""")

wrong_conf_file_content = string.Template("""
parameters {
  server {
    address = '127.0.0.1'
    port = $port
    cache_dir= "$cache_dir"
  }
}

actions {
  blacklist {
     command = blacklist("$$ip")
  }
}

rules {
  AUTH [
    ignore "[a-zA-Z]+(?:\\(\\w+\\))?\\[[0-9]*\\]: session opened"
    blacklist "sshd\\[[0-9]*\\]: Failed password for illegal user (\\w*"
  ]
}
""")

############################################################

def read_file(filename):
    fh = open(filename, 'r')
    try:
        return fh.read().strip()
    finally:
        fh.close()
    return None

def read_file_lines(filename):
    lines = []
    fh = open(filename, 'r')
    try:
        for line in fh:
            lines.append(line.rstrip())
    finally:
        fh.close()
    return lines

def write_file(filename, content):
    fh = open(filename, 'w')
    try:
        fh.write(content)
    finally:
        fh.close()

def wait_for_file(filename, timeout=5):
    while timeout > 0:
        if os.path.isfile(filename):
            return True
        time.sleep(1)
        timeout -= 1
    return False


class FakeSyslog:

    """Simulate a syslog daemon."""

    pri_notice = 5

    ip_address = '127.0.0.1'
    port = 5140

    def __init__(self, messages):
        self.messages = messages
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.cond = threading.Condition()
        self.count = len(messages)

    def close(self):
        if self.sock:
            self.sock.close()
            self.sock = None

    def send(self, data):
        self.sock.sendto(data, (self.ip_address, self.port))

    def send_messages(self, messages):
        for facility, ignore, tag, msg in messages:
            now = time.strftime('%b %d %X')
            data = '<%s>%s %s %s'%(facility * 8 + self.pri_notice, now, 'localhost', msg)
            self.send(data.encode('UTF-8'))
            time.sleep(0.2)

    def notify(self, what):
        if what == 'start':
            with self.cond:
                self.cond.notify()
        elif what == 'data':
            self.count -= 1
            return self.count > 0
        return True

    def __call__(self):
        with self.cond:
            self.cond.wait()
            self.send_messages(self.messages)


class TestRun(unittest.TestCase):

    def setUp(self):
        self.working_dir = working_dir = tempfile.mkdtemp(prefix='tmp_slogguard')
        self.cache_dir = os.path.join(working_dir, 'cache')
        os.mkdir(self.cache_dir)
        self.action_file = os.path.join(working_dir, 'slogguard.py')
        write_file(self.action_file, action_file_content.substitute(working_dir=working_dir))
        self.conf_file = os.path.join(working_dir, 'slogguard.conf')

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    def writeConfigFile(self, content):
        write_file(self.conf_file, content.substitute(cache_dir=self.cache_dir, port=FakeSyslog.port))

    def runSlogguard(self, extra_args=[], verbosity='quiet', expected_code=0):
        exit_code = 255
        try:
            args = [ '--{}'.format(verbosity), '--config-file', self.conf_file, '--plugin-file', self.action_file ]
            slogguard.main(args + extra_args)
        except SystemExit as ex:
            exit_code = ex.code
        self.assertEqual(expected_code, exit_code)

    def test_checkConfig(self):
        """Check the configuration file and exit"""
        for verbosity, report_error in [ ('quiet', 'logging.Logger.error'), ('debug', 'traceback.print_exc') ]:
            with unittest.mock.patch('logging.Logger.info'):
                with unittest.mock.patch('logging.Logger.debug'):
                    # Correct file
                    self.writeConfigFile(conf_file_content)
                    self.runSlogguard([ '--check-config' ], verbosity=verbosity)
                    # Wrong file
                    self.writeConfigFile(wrong_conf_file_content)
                    with unittest.mock.patch(report_error) as mock_error:
                        self.runSlogguard([ '--check-config' ], verbosity=verbosity, expected_code=1)
                        assert_called_once(mock_error)

    def test_runCommand(self):
        """Run the command"""
        self.writeConfigFile(conf_file_content)

        with contextlib.closing(FakeSyslog(log_lines)) as syslogger:
            slogguard.processor.ServerLoop.callback = syslogger.notify
            logger_thread = threading.Thread(target=syslogger)
            logger_thread.start()
            with unittest.mock.patch('subprocess.call') as mock_call:
                self.runSlogguard()
                mock_call.assert_called()
            logger_thread.join()

        result_file = os.path.join(self.working_dir, 'blacklist.txt')
        has_result = os.path.isfile(result_file)
        self.assertTrue(has_result, "{}: file not found: (content: {})".format(result_file, ', '.join(os.listdir(self.working_dir))))
        if has_result:
            content = read_file_lines(result_file)
            self.assertEqual(content, [ '74.125.126.86', '2001:db8::0:1:0:0:1', '10.0.23.65' ])

        cache_conditions = { 'auth': (4 , 'CRON'), 'daemon': (1, 'smartctl: ') }
        for facility in cache_conditions:
            cache_file = os.path.join(self.cache_dir, facility)
            has_cache = os.path.isfile(cache_file)
            self.assertTrue(has_cache)
            if has_cache:
                content = read_file_lines(cache_file)
                nlines, prefix = cache_conditions[facility]
                self.assertEqual(len(content), nlines)
                if len(content) == nlines:
                    for line in content:
                        cols = line.split(' ', 3)
                        self.assertEqual(len(cols), 4)
                        if len(cols) == 4:
                            self.assertTrue(cols[3].startswith(prefix))


if __name__ == "__main__":
    unittest.main()

