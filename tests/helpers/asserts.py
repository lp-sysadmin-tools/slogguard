#! /usr/bin/python -B

# Copyright (C) 2011-2020 Laurent Pelecq <lpelecq+slogguard@circoise.eu>

# This file is part of Slogguard.
#
# Slogguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__all__ = [ 'assert_called_once' ]

def assert_called_once(mock):
    try:
        mock.assert_called_once()
    except AttributeError:
        # python < 3.6
        if mock.call_count != 1:
            raise AssertionError('Expected {} to be called once instead of {} times'.format(mock.__name__, mock.call_count))
