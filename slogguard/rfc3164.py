# Copyright (C) 2011-2017 Laurent Pelecq <lpelecq+slogguard@circoise.eu>

# This file is part of Slogguard.
#
# Slogguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__all__ = [ 'FACILITY_KERNEL', 'FACILITY_USER', 'FACILITY_MAIL', 'FACILITY_DAEMON', 'FACILITY_AUTH',
            'FACILITY_INTERNAL', 'FACILITY_PRINTER', 'FACILITY_NEWS', 'FACILITY_UUCP', 'FACILITY_CLOCK',
            'FACILITY_AUTH2', 'FACILITY_FTP', 'FACILITY_NTP', 'FACILITY_AUDIT', 'FACILITY_ALERT',
            'FACILITY_CLOCK2', 'FACILITY_LOCAL0', 'FACILITY_LOCAL1', 'FACILITY_LOCAL2', 'FACILITY_LOCAL3',
            'FACILITY_LOCAL4', 'FACILITY_LOCAL5', 'FACILITY_LOCAL6', 'FACILITY_LOCAL7',
            'facility_name' ]

FACILITY_KERNEL   = 0
FACILITY_USER     = 1
FACILITY_MAIL     = 2
FACILITY_DAEMON   = 3
FACILITY_AUTH     = 4
FACILITY_INTERNAL = 5
FACILITY_PRINTER  = 6
FACILITY_NEWS     = 7
FACILITY_UUCP     = 8
FACILITY_CLOCK    = 9
FACILITY_AUTH2    = 10
FACILITY_FTP      = 11
FACILITY_NTP      = 12
FACILITY_AUDIT    = 13
FACILITY_ALERT    = 14
FACILITY_CLOCK2   = 15
FACILITY_LOCAL0   = 16
FACILITY_LOCAL1   = 17
FACILITY_LOCAL2   = 18
FACILITY_LOCAL3   = 19
FACILITY_LOCAL4   = 20
FACILITY_LOCAL5   = 21
FACILITY_LOCAL6   = 22
FACILITY_LOCAL7   = 23

facility_names = {
    FACILITY_KERNEL: 'kernel',
    FACILITY_USER:   'user',
    FACILITY_MAIL:   'mail',
    FACILITY_DAEMON: 'daemon',
    FACILITY_AUTH:   'auth',
    FACILITY_INTERNAL: 'internal',
    FACILITY_PRINTER: 'printer',
    FACILITY_NEWS:   'news',
    FACILITY_UUCP:   'uucp',
    FACILITY_CLOCK:  'clock',
    FACILITY_AUTH2:  'auth2',
    FACILITY_FTP:    'ftp',
    FACILITY_NTP:    'ntp',
    FACILITY_AUDIT:  'audit',
    FACILITY_ALERT:  'alert',
    FACILITY_CLOCK2: 'clock2',
    FACILITY_LOCAL0: 'local0',
    FACILITY_LOCAL1: 'local1',
    FACILITY_LOCAL2: 'local2',
    FACILITY_LOCAL3: 'local3',
    FACILITY_LOCAL4: 'local4',
    FACILITY_LOCAL5: 'local5',
    FACILITY_LOCAL6: 'local6',
    FACILITY_LOCAL7: 'local7'
    }

def facility_name(value):
    try:
        return facility_names[value]
    except KeyError:
        return '<unknown-facility>'
