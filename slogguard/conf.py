# Copyright (C) 2011-2017 Laurent Pelecq <lpelecq+slogguard@circoise.eu>

# This file is part of Slogguard.
#
# Slogguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__all__ = [ 'read_config_file' ]

import io
import re
import shlex
import collections

from slogguard.rfc3164 import *
from slogguard.log import *

class SyntaxError(Exception):
    pass

class EndOfInputException(Exception):
    pass

class Command:

    def __init__(self, func=None, args=None):
        super(Command, self).__init__()
        self.func = func
        self.args = args or []

class Struct(collections.UserDict):

    def __getattr__(self, key):
        if key in self.data:
            return self.data[key]
        return super().__getattr__(key)

class MutableStruct:
    pass

class Action(MutableStruct):

    __slots__ = ('command', 'session', 'expire', 'threshold')

    def __init__(self, command, session, expire, threshold):
        super().__init__()
        self.command = command
        self.session = session
        self.expire = expire
        self.threshold = threshold

class Server(MutableStruct):

    __slots__ = ('address', 'port', 'cache_dir')

    def __init__(self, address, port, cache_dir=None):
        super().__init__()
        self.address = address
        self.port = port
        self.cache_dir = cache_dir

class Mail(MutableStruct):

    __slots__ = ('host', 'delay', 'sender', 'recipients')

    def __init__(self, host, delay, sender, recipients):
        super().__init__()
        self.host = host
        self.delay = delay
        self.sender = sender
        self.recipients = recipients

class Logging(MutableStruct):

    __slots__ = ('output', 'size', 'keep')

    def __init__(self, output, size, keep):
        super().__init__()
        self.output = output
        self.size = size
        self.keep = keep

class Actions(collections.UserDict):

    """Set of actions."""

    def new_value(self, key):
        """Return a new action if it hasn't been set before"""
        if not key in self:
            self[key] = Action(Command(), None, 0, 0)
        return self[key]

Rule = collections.namedtuple('Rule', [ 'regexp', 'pattern', 'action' ])

def make_config():
    return Struct(
        parameters=Struct(
            server=Server('127.0.0.1', 514, None),
            mail=Mail('localhost', 10, '', []),
            logging=Logging('stream', 500, 3)
        ),
        rules={})

class Placeholders:

    """Replace placeholders by they values.

    Placeholders syntax is %{name}. Parameters must include a value for 'name'."""

    def __init__(self, parameters):
        self.parameters = parameters
        self.ph_regexp = re.compile(r'%\{(\w+)\}')

    def __call__(self, m):
        key = m.group(1)
        if not key in self.parameters:
            raise Exception('{}: unknown placeholder'.format(key))
        return self.parameters[key]

    def replace(self, s):
        return self.ph_regexp.sub(self, s)

class ConfigFileParser(object):

    """Parse configuration files."""

    facilities = {
        'KERNEL': FACILITY_KERNEL,
        'USER': FACILITY_USER,
        'MAIL': FACILITY_MAIL,
        'DAEMON': FACILITY_DAEMON,
        'AUTH': FACILITY_AUTH,
        'INTERNAL': FACILITY_INTERNAL,
        'PRINTER': FACILITY_PRINTER,
        'NEWS': FACILITY_NEWS,
        'UUCP': FACILITY_UUCP,
        'CLOCK': FACILITY_CLOCK,
        'FTP': FACILITY_FTP,
        'NTP': FACILITY_NTP,
        'AUDIT': FACILITY_AUDIT,
        'ALERT': FACILITY_ALERT,
        'LOCAL0': FACILITY_LOCAL0,
        'LOCAL1': FACILITY_LOCAL1,
        'LOCAL2': FACILITY_LOCAL2,
        'LOCAL3': FACILITY_LOCAL3,
        'LOCAL4': FACILITY_LOCAL4,
        'LOCAL5': FACILITY_LOCAL5,
        'LOCAL6': FACILITY_LOCAL6,
        'LOCAL7': FACILITY_LOCAL7
        }

    def __init__(self, fh, filename=None, commands={}):
        super(ConfigFileParser, self).__init__()
        self.ins = shlex.shlex(fh, posix=True)
        self.filename = filename
        self.commands = commands

    def syntax_error(self, msg):
        if self.filename:
            msg = '{name}:{pos}: {msg}'.format(name=self.filename, pos=self.ins.lineno, msg=msg)
        else:
            msg = 'at line {pos}: {msg}'.format(pos=self.ins.lineno, msg=msg)
        return SyntaxError(msg)

    def next_token(self):
        token = self.ins.get_token()
        if not token:
            raise EndOfInputException()
        return token

    def eat(self, expected_tokens):
        while expected_tokens:
            expected = expected_tokens.pop(0)
            token = self.next_token()
            if token != expected:
                raise self.syntax_error('invalid input read: "{token}" instead of "{expected}"'.format(token=token, expected=expected))

    def parse_string_list(self):
        self.eat(['['])
        l = []
        while True:
            token  = self.next_token()
            if token == ']':
                break
            l.append(token)
        return l

    def parse_string_dict(self):
        self.eat(['{'])
        d = {}
        while True:
            token  = self.next_token()
            if token == '}':
                break
            key = token
            self.eat(['='])
            value = self.next_token()
            if token == '}':
                raise self.syntax_error("unexpected closing brace")
            d[key] = value
        return d

    def parse_command(self, command):
        token  = self.next_token()
        if not token in self.commands:
            raise self.syntax_error("{}: unknown command".format(token))
        command.func = self.commands[token]
        self.eat(['('])
        token  = self.next_token()
        if token != ')':
            while True:
                if token == ',':
                    raise self.syntax_error('unexpected comma in command parameters list')
                command.args.append(token)
                token  = self.next_token()
                if token == ')':
                    break
                elif token != ',':
                    raise self.syntax_error('expecting comma or closing parenthesis in command parameters list')
                token = self.next_token()

    def parse_object(self, d):
        """Parse an object with predefined attributes.

        The type of default values are used to infer the type of attribute values."""
        self.eat(['{'])
        while True:
            token  = self.next_token()
            if token == '}':
                break
            key = token
            old_value = None
            if hasattr(d, 'new_value'):
                old_value = d.new_value(key)
            else:
                try:
                    old_value = getattr(d, key)
                except AttributeError:
                    raise self.syntax_error("{}: invalid parameter".format(key))
            value = None
            if isinstance(old_value, tuple) or isinstance(old_value, MutableStruct):
                self.parse_object(old_value)
            elif isinstance(old_value, Command):
                self.eat(['='])
                self.parse_command(old_value)
            else:
                self.eat(['='])
                if isinstance(old_value, list):
                    value = self.parse_string_list()
                elif isinstance(old_value, int):
                    try:
                        value = int(self.next_token())
                    except ValueError:
                        raise self.syntax_error("{}: parameter must be an integer".format(key))
                elif not old_value or isinstance(old_value, str):
                    value = self.next_token()
                else:
                    raise self.syntax_error('internal error: {}: unknown type'.format(type(old_value)))
                try:
                    setattr(d, key, value)
                except AttributeError as ex:
                    raise self.syntax_error('internal error: {}: {}'.format(key, ex))

    def parse_facility(self, facility, actions, placeholders):
        self.eat(['['])
        while True:
            token  = self.next_token()
            if token == ']':
                break
            if not token in actions:
                raise self.syntax_error('{name}: unknown action (available: {actions})'.format(name=token, actions=', '.join([ x for x in actions ])))
            action = actions[token]
            pattern = self.next_token()
            if placeholders:
                try:
                    pattern = placeholders.replace(pattern)
                except Exception as ex:
                    raise self.syntax_error(str(ex))
            try:
                regexp = re.compile(pattern)
            except re.error as ex:
                raise self.syntax_error("invalid pattern: {}".format(ex))
            logger.debug("rule with pattern '{}' and action '{}'".format(pattern, token))
            facility.append(Rule(regexp, pattern, action))

    def parse_rules(self, rules, actions):
        self.eat(['{'])
        placeholders = None
        while True:
            token  = self.next_token()
            if token == '}':
                break
            if not rules and token == '%':
                placeholders = Placeholders(self.parse_string_dict())
            elif not token in self.facilities:
                raise self.syntax_error("{}: invalid syslog facility".format(token))
            else:
                logger.debug("rules for facility '{}'".format(token))
                rules[self.facilities[token]] = facility = []
                self.parse_facility(facility, actions, placeholders)

    def parse_config(self, cfg):
        actions = Actions()
        actions['ignore'] = None
        while True:
            token = self.next_token()
            try:
                if token == 'parameters':
                    self.parse_object(cfg.parameters)
                elif token == 'actions':
                    self.parse_object(actions)
                elif token == 'rules':
                    self.parse_rules(cfg.rules, actions)
                else:
                    raise self.syntax_error("{}: invalid parameter".format(token))
            except EndOfInputException:
                raise self.syntax_error('premature end of input')

    def __call__(self):
        cfg = make_config()
        try:
            self.parse_config(cfg)
        except EndOfInputException:
            pass
        return cfg


def read_config_file(filename, commands):
    with open(filename) as fh:
        parse = ConfigFileParser(fh, filename, commands)
        return parse()


if __name__ == '__main__':
    import sys
    import logging
    root = logging.getLogger()
    hdlr = logging.StreamHandler()
    fmt = '%(levelname)s: %(message)s'
    hdlr.setFormatter(logging.Formatter(fmt))
    root.addHandler(hdlr)
    root.setLevel(logging.INFO)
    cfg = read_config_file(sys.argv[1])
    print(str(cfg))
