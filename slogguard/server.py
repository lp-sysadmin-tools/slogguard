# Copyright (C) 2015 Laurent Pelecq <lpelecq+slogguard@circoise.eu>

# This file is part of Slogguard.
#
# Slogguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import errno
import socket
import select

try:
    import systemd.daemon
except ImportError:
    pass

class Listener:

    MAX_PACKET_SIZE = 8192

    def __init__(self, address, timeout):
        self.timeout = timeout
        if address == 'systemd':
            fds = systemd.daemon.listen_fds()
            self.sockets = []
            for fd in fds:
                sock = None
                if systemd.daemon.is_socket_unix(fd):
                    family = socket.AF_UNIX
                else:
                    family = socket.AF_INET
                sock = socket.fromfd(fd, family, socket.SOCK_DGRAM)
                self.sockets.append(sock)
        else:
            if isinstance(address, tuple):
                family = socket.AF_INET
            else:
                family = socket.AF_UNIX
            sock = socket.socket(family, socket.SOCK_DGRAM)
            try:
                sock.bind(address)
            except:
                sock.close()
                raise
            self.sockets = [ sock ]

    def close(self):
        try:
            while self.sockets:
                self.sockets.pop().close()
        except:
            pass

    def get_data(self):
        try:
            fd_sets = select.select(self.sockets, [], [], self.timeout)
            if not fd_sets[0]:
                return (None, None)
            else:
                for rfd in fd_sets[0]:
                    return rfd.recvfrom(self.MAX_PACKET_SIZE)
        except OSError as ex:
            if ex.errno == errno.EINTR:
                return (None, None)
            self.close()
            raise
