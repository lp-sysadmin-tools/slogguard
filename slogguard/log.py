# Copyright (C) 2011-2017 Laurent Pelecq <lpelecq+slogguard@circoise.eu>

# This file is part of Slogguard.
#
# Slogguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__all__ = [ 'logger' ]

import logging
import logging.handlers

class LoggerHolder(object):

    """Logging configuration."""

    DEBUG = logging.DEBUG
    VERBOSE = logging.INFO
    QUIET = logging.WARNING

    def __init__(self):
        super(LoggerHolder, self).__init__()
        self.logger = logging.getLogger('slogguard')

    def initialize(self, level):
        self.logging_handler = logging.StreamHandler()
        self.logging_handler.setFormatter(logging.Formatter('%(levelname)s: %(message)s'))
        self.logger.addHandler(self.logging_handler)
        self.default_level = self.current_level = level
        self.logger.setLevel(level)
        self.kind = 'stream'

    def toggle_debug(self):
        if self.current_level == self.default_level:
            self.current_level = self.DEBUG
        else:
            self.current_level = self.default_level
        self.logger.setLevel(self.current_level)

    def enable_file(self, filename, size_kb, keep):
        logging_handler = logging.handlers.RotatingFileHandler(filename, maxBytes=size_kb * 1024, backupCount=keep)
        logging_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))
        self.logger.addHandler(logging_handler)
        self.logger.removeHandler(self.logging_handler)
        self.logging_handler = logging_handler
        self.kind = 'file'

    def enable_syslog(self):
        logging_handler = logging.handlers.SysLogHandler('/dev/log', logging.handlers.SysLogHandler.LOG_DAEMON)
        logging_handler.setFormatter(logging.Formatter('%(name)s: %(levelname)s: %(message)s'))
        self.logger.addHandler(logging_handler)
        self.logger.removeHandler(self.logging_handler)
        self.logging_handler = logging_handler
        self.kind = 'syslog'

log_admin = LoggerHolder()
logger = log_admin.logger
