# Copyright (C) 2011-2020 Laurent Pelecq <lpelecq+slogguard@circoise.eu>

# This file is part of Slogguard.
#
# Slogguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__all__ = [ 'SyslogProcessor', 'Daemon', 'read_config_file', 'logger', '__version__' ]

import sys
import os, signal
from contextlib import closing
import time
import threading
import io

import smtplib

from .version import __version__
import slogguard.handler
import slogguard.conf
import slogguard.server
from .rfc3164 import *

from .log import logger, log_admin

try:
    import systemd.daemon

    def notify_systemd():
        if systemd.daemon.booted():
            systemd.daemon.notify("READY=1")

except ImportError:

    def notify_systemd():
        pass

def read_config_file(filename, actions):
    cfg = slogguard.conf.read_config_file(filename, actions)
    rules = {}
    make_matcher = slogguard.handler.Matcher.make
    for facility in cfg.rules:
        rules[facility] = [ make_matcher(x) for x in cfg.rules[facility] ]
    cfg.rules = rules
    return cfg


class ServerLoop:

    MIN_SYSLOG_MSG_LENGTH = 3 + 15 + 1 # PRI + TIMESTAMP + SPC

    SYSLOG_TIMESTAMP_LENGTH = 15

    callback = None # for unit tests

    def __init__(self, address, timeout):
        self.address = address
        self.timeout = timeout
        self.shutdown_thread = None
        self.must_stop = False
        self.toggle_debug = False

    def start(self):
        try:
            logger.info('listening on {0}'.format(str(self.address)))
            with closing(slogguard.server.Listener(self.address, self.timeout)) as listener:
                if self.callback:
                    self.callback('start')
                while not self.must_stop:
                    data, client_address = listener.get_data()
                    if self.toggle_debug:
                        self.toggle_debug = False
                        logger.toggle_debug()
                    if data:
                        self.process_data(data)
                        if self.callback and not self.callback('data'):
                            break
                    else:
                        self.handle_timeout()
        except KeyboardInterrupt:
            pass
        except Exception as ex:
            logger.error(str(ex))
        if self.shutdown_thread:
            self.shutdown_thread.join()
            self.shutdown_thread = None

    def __shutdown(self):
        self.must_stop = True

    def stop(self):
        self.shutdown_thread = threading.Thread(target=self.__shutdown)
        self.shutdown_thread.start()


class SyslogProcessor(ServerLoop):

    """Process syslog messages and send mails."""

    TIMEOUT = 60.0

    def __init__(self, cfg):
        super().__init__(self.get_address(cfg), self.TIMEOUT)
        self.cache_dir = cfg.parameters.server.cache_dir
        self.mail_delay = cfg.parameters.mail.delay * 60 # seconds
        self.mail_host = cfg.parameters.mail.host
        self.mail_sender = cfg.parameters.mail.sender
        self.mail_recipients = cfg.parameters.mail.recipients
        self.rules = cfg.rules
        self.mail_timestamp = time.time()
        logger.info('facilities: {}'.format(', '.join(sorted([ facility_name(x) for x in self.rules ]))))


    @classmethod
    def decode_message(self, data):
        if len(data) < self.MIN_SYSLOG_MSG_LENGTH:
            raise Exception('syslog message too short')
        elif data[0] != '<':
            raise Exception("syslog message doesn't start with PRI")
        pri_len = data.find('>')
        if pri_len <= 0:
            raise Exception('invalid PRI in syslog message')
        pri = int(data[1:pri_len])
        pri_end = pri_len + 1
        facility = int(pri / 8)
        if facility == FACILITY_AUTH2:
            facility = FACILITY_AUTH
        elif facility == FACILITY_CLOCK2:
            facility = FACILITY_CLOCK
        severity = pri % 8
        time_end = pri_end + self.SYSLOG_TIMESTAMP_LENGTH
        timestamp = data[pri_end:time_end]
        hostname, msg = data[time_end + 1:].split(' ', 1)
        return facility, severity, timestamp, hostname, msg

    @classmethod
    def get_address(self, cfg):
        address = cfg.parameters.server.address
        if address == 'systemd' or os.path.isabs(address):
            return address
        return (address, cfg.parameters.server.port)

    def rfc2882date(self):
        return time.strftime("%a, %d %b %Y %X GMT", time.gmtime())

    def send_mail(self):
        if self.mail_host and self.mail_sender and self.mail_recipients:
            msg = io.StringIO()
            msg.write("From: {}\r\n".format(self.mail_sender))
            msg.write("To: {}\r\n".format(", ".join(self.mail_recipients)))
            msg.write("Subject: syslog messages\r\n")
            msg.write("Date: {}\r\n".format(self.rfc2882date()))
            msg.write("\r\n")
            sep = ''
            nlines = 0
            for filename in os.listdir(self.cache_dir):
                if filename != '.' and filename != '..':
                    filepath = os.path.join(self.cache_dir, filename)
                    msg.write(sep)
                    msg.write('[{}]\n'.format(filename))
                    with io.open(filepath) as fh:
                        for line in fh:
                            msg.write(line)
                            nlines += 1
                    sep = '\n'
                    os.unlink(filepath)
            if nlines > 0:
                client = smtplib.SMTP(self.mail_host)
                client.sendmail(self.mail_sender, self.mail_recipients, msg.getvalue())
                client.quit()

    def periodic_tasks(self):
        now = time.time()
        if now - self.mail_timestamp > self.mail_delay:
            self.send_mail()
            self.mail_timestamp = now

    def process_data(self, data):
        facility, severity, timestamp, hostname, msg = self.decode_message(data.decode('UTF-8'))
        logger.debug('processing message: [{}] {}'.format(facility, msg))
        self.process_syslog_message(facility, severity, timestamp, hostname, msg)

    def process_syslog_message(self, facility, severity, timestamp, hostname, msg):
        if facility in self.rules:
            for matcher in self.rules[facility]:
                if matcher(msg):
                    msg = None
                    logger.debug("message matched in {}: {}".format(facility_name(facility), matcher.pattern))
                    break
                else:
                    logger.debug("message don't match in {}: {}".format(facility_name(facility), matcher.pattern))
            if msg:
                logger.debug('message not matched in facility: {}'.format(facility_name(facility)))
                cache_file = os.path.join(self.cache_dir, slogguard.conf.facility_name(facility))
                logger.debug('writing to cache: {}'.format(cache_file))
                with io.open(cache_file, 'a') as fh:
                    fh.write('{ts} {msg}\n'.format(ts=timestamp, msg=msg))
        self.periodic_tasks()

    def handle_timeout(self):
        self.periodic_tasks()


class DaemonError(Exception):
    pass

class Daemon(object):

    """Base class for a UNIX daemon."""

    def __init__(self, factory, pid_file=None, no_output=False):
        super(Daemon, self).__init__()
        self.factory = factory
        self.application = None
        self.pid_file = pid_file
        self.no_output = no_output

    def is_daemon(self):
        return self.pid_file != None

    def signal_handler(self, signum, frame):
        if signum == signal.SIGTERM:
            logger.info('stopping on TERM signal')
            self.stop()
        elif signum == signal.SIGUSR1:
            if self.application:
                self.application.toggle_debug = True

    def read_pid(self):
        with io.open(self.pid_file) as fh:
            pid = int(fh.read().strip())
            return pid

    def process_exists(self, pid):
        """Checks if a process exists.

        On Linux, it checks if /proc/<pid> exists. Otherwise it sends
        signal 0."""
        if os.path.isdir('/proc'):
            return os.path.exists(os.path.join('/proc', str(pid)))
        else:
            try:
                os.kill(pid, 0)
                return True
            except OSError:
                pass
        return False

    def start_processing(self):
        signal.signal(signal.SIGTERM, self.signal_handler)
        signal.signal(signal.SIGUSR1, self.signal_handler)
        logging_config = self.factory.cfg.parameters.logging
        if not logging_config.output == 'stream':
            if self.no_output:
                sys.stdout.close()
                sys.stderr.close()
            if logging_config.output == 'syslog':
                log_admin.enable_syslog()
            else:
                log_admin.enable_file(logging_config.output, logging_config.size, logging_config.keep)
        self.application = self.factory()
        self.application.start()

    def start(self):
        """Start the application."""

        if self.is_daemon():
            if os.path.isfile(self.pid_file):
                pid = self.read_pid()
                if self.process_exists(pid):
                    raise DaemonError('process already exists with pid {}'.format(pid))

            if os.fork() == 0:
                sys.stdin.close()
                os.setsid()
                child_pid = os.fork()
                if child_pid == 0:
                    self.start_processing()
                else:
                    with io.open(self.pid_file, "w") as fh:
                        fh.write('{}\n'.format(child_pid))
        else:
            self.start_processing()

    def exit(self, exit_code=0):
        if isfile(self.pid_file):
            pid = self.read_pid()
            if pid == os.getpid():
                os.unlink(self.pid_file)
        sys.exit(exit_code)

    def stop(self):
        """Stop the process."""
        try:
            if self.application:
                self.application.stop()
        except Exception as ex:
            logger.error(str(ex))
        except:
            logger.error('{}\n'.format(sys.exc_info()[1]))
