#! /usr/bin/env python

# Copyright (C) 2011-2017 Laurent Pelecq <lpelecq+slogguard@circoise.eu>

# This file is part of Slogguard.
#
# Slogguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import io
import os
import subprocess
import sys

from .processor import *
from .log import log_admin

### Parsing command line

usage="""%prog [options]

Listen for syslog messages and execute actions accordingly."""


############################################################
### Main

from os.path import dirname, abspath, isdir

class SyslogProcessorFactory(object):

    def __init__(self, cfg):
        super(SyslogProcessorFactory, self).__init__()
        self.cfg = cfg

    def __call__(self):
        return SyslogProcessor(self.cfg)

def exec_command(*args):
    subprocess.call(args)

def run(args):
    log_level = log_admin.VERBOSE
    if args.quiet:
        log_level = log_admin.QUIET
    elif args.debug:
        log_level = log_admin.DEBUG
    log_admin.initialize(log_level)

    no_output = not args.debug

    actions = {}
    for filename in args.plugins:
        with io.open(filename) as fh:
            logger.info('reading action file {}'.format(filename))
            exec(compile(fh.read(), filename, 'exec'), globals(), actions)

    actions['log'] = logger.info
    actions['exec'] = exec_command

    logger.info('reading config file {}'.format(args.config_file))
    cfg = read_config_file(args.config_file, actions)
    if cfg and not args.check_config:
        factory = SyslogProcessorFactory(cfg)
        daemon = Daemon(factory, args.pid_file, no_output)
        daemon.start()
        logger.info('exiting')

def main(argv=None):
    debug = False
    try:
        argparser = argparse.ArgumentParser(description='Listen for syslog messages and execute actions accordingly.')

        argparser.add_argument('--debug', action="store_true", default=False,
                               help="Debug mode, for developers only")

        argparser.add_argument("-q", "--quiet",
                               dest="quiet", action="store_true", default=False,
                               help="print only warning and error messages")

        argparser.add_argument("-c", "--config-file",
                               dest="config_file", action="store", default=None,
                               metavar="FILE", help="read this configuration file.")

        argparser.add_argument("-C", "--check-config",
                               dest="check_config", action="store_true", default=False,
                               help="check the configuration file and exit.")

        argparser.add_argument("-p", "--plugin-file",
                               dest="plugins", action="append", default=[],
                               metavar="FILE", help="additional plugin files that defines actions.")

        argparser.add_argument("-P", "--pid-file",
                               dest="pid_file", action="store", default=None,
                               metavar="FILE", help="PID file to start as daemon")

        argparser.add_argument('-V', '--version',
                               action='version',
                               version='%(prog)s -- version {0}'.format(__version__))

        args = argparser.parse_args(args=argv)
        debug = args.debug
        run(args)
    except:
        if debug:
            import traceback
            traceback.print_exc()
        else:
            logger.error('{}\n'.format(sys.exc_info()[1]))
        sys.exit(1)
    sys.exit(0)

#  Local Variables: ***
#  mode: python ***
#  End: ***
