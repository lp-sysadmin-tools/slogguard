# Copyright (C) 2011-2015 Laurent Pelecq <lpelecq+slogguard@circoise.eu>

# This file is part of Slogguard.
#
# Slogguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__all__ = [ 'Matcher' ]

import string, re
import time
from slogguard.log import *


class Action:

    def __init__(self, func, args):
        super().__init__()
        self.func = func
        self.args = [ string.Template(x) for x in args ]

    def __call__(self, match_data):
        if self.func:
            args = [ x.substitute(match_data) for x in self.args ]
            self.func(*tuple(args))


class Session:

    def __init__(self):
        super().__init__()
        self.count = 1
        self.start_time = time.time()

    def increase(self):
        self.count += 1
        return self.count

    def has_expired(self, delay):
        return time.time() - self.start_time > delay


class Matcher:

    """Execute action if line matchs.

    Maintains a dictionary of sessions if the following parameters are set:
      session:   a template to build session id, parameters are pattern subexpression names,
      threshold: the number of matches before executing the action,
      expire:    the session lifetime in seconds

    Example:
      if pattern is "authentication failed for (?P<name>[a-z]+)", the session id may be
      equal to "auth-$name".
    """

    def __init__(self, regexp, pattern, action=None, session=None, threshold=None, expire=None):
        super().__init__()
        self.regexp = regexp
        self.pattern = pattern
        self.action = action
        self.threshold = self.expire = None
        if session and isinstance(threshold, int) and threshold >= 0 and isinstance(expire, int) and expire > 0:
            self.session_id_format = string.Template(session)
            self.threshold = threshold
            self.expire = expire
            if self.session_id_format and self.threshold and self.expire:
                self.sessions = {}

    def __action(self, match_data):
        if self.action:
            try:
                self.action(match_data)
            except Exception as ex:
                logger.error('unexpected exception in action {name}: {ex}'.format(name=self.action, ex=ex))

    def __call__(self, line):
        m =  self.regexp.match(line)
        if m:
            if self.expire:
                sessions = self.sessions
                to_be_deleted = []
                for session_id in sessions:
                    if sessions[session_id].has_expired(self.expire):
                        to_be_deleted.append(session_id)
                for session_id in to_be_deleted:
                    logger.debug('session "{}" has expired'.format(session_id))
                    del sessions[session_id]
                match_data = m.groupdict()
                session_id = self.session_id_format.substitute(match_data)
                if session_id in sessions:
                    count = sessions[session_id].increase()
                    if count >= self.threshold:
                        logger.debug('executing action on session "{id}" ({count} > {max})'.format(id=session_id, count=count, max=self.threshold))
                        self.__action(match_data)
                        del sessions[session_id]
                    else:
                        logger.debug('threshold not reached on session "{id}" ({count} <= {max})'.format(id=session_id, count=count, max=self.threshold))
                else:
                    logger.debug('new session "{id}" (threshold {max})'.format(id=session_id, max=self.threshold))
                    sessions[session_id] = Session()
            else:
                self.__action(m.groupdict())
            return True
        return False

    @staticmethod
    def make(conf):
        if not conf.action:
            return Matcher(conf.regexp, conf.pattern)
        action = Action(conf.action.command.func, conf.action.command.args)
        session = conf.action.session
        threshold = conf.action.threshold
        expire = conf.action.expire
        return Matcher(conf.regexp, conf.pattern, action, session, threshold, expire)
