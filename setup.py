#!/usr/bin/env python

from setuptools import setup

import sys, os, io
with io.open(os.path.join(os.path.dirname(sys.argv[0]), 'slogguard', 'version.py')) as infh:
    exec(infh.read())

setup(name='slogguard',
      version=__version__,
      description='Syslog messages filter',
      long_description = "Listen for syslog messages and execute actions accordingly.",
      author='Laurent Pelecq',
      author_email='lpelecq+slogguard@circoise.eu',
      entry_points = {
          "console_scripts": ['slogguard = slogguard:main']
      },
      packages=['slogguard'],
      test_suite="tests",
      license='GNU General Public License version 3'
      )
