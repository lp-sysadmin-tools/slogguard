Slogguard
=========

[![build status](https://gitlab.com/lp-sysadmin-tools/slogguard/badges/master/pipeline.svg)](https://gitlab.com/lp-sysadmin-tools/slogguard/commits/master)
[![license](https://img.shields.io/badge/license-GPL3-lightgrey.svg)](https://gitlab.com/lp-sysadmin-tools/slogguard/raw/master/LICENSE)

Overview
--------

Log file monitoring.

- Parse regular syslog format or run as a syslog network client of systemd.

- Execute actions when a pattern match. Actions can be executed only if a given
  pattern occurs multiple times in a time frame.

- Send all entries that haven't be processed by mail.

Installation
------------

To install in <DIR>, run

    $ python setup.py install --prefix <DIR>

or

    $ make INSTALL_DIR=<DIR> install


Configuration
-------------

Syslog must be configured to forward messages to an UDP port. For example, with rsyslog:

    auth.* @127.0.0.1:5140
    daemon.* @127.0.0.1:5140

With systemd, the service must be an alias of syslog.service. See
[Writing syslog Daemons Which Cooperate Nicely With systemd](https://www.freedesktop.org/wiki/Software/systemd/syslog/) for details. There is an example of service definition in folder *contrib*.

Example of config file:

    parameters {
      # Listen for syslog UDP messages on port 5140
      server {
        address = '127.0.0.1'
        port = 5140
        cache_dir= "/var/cache/slogguard"
      }

      # Unmatched messages are sent by mail every 10 minutes.
      mail {
        delay = 10 # minutes
        host = "localhost"
        sender = "daemon@domain.net"
        recipients = [ "root@domain.net" "postmaster@domain.net" ]
      }

      # Log self messages in a file
      logging {
        output = "/var/log/slogguard.log"
        size = 1000 # kb
        keep = 5
      }
    }

    actions {
      # Log a message when there were 3 unsuccessful login attempts from the same host
      # The session is the unique identifier to count how many times the event matched
      login_failure {
        command = log("there have been 3 unsuccessful login attempts from $ip (last user was $user)")
        session = "ssh-$ip"
        expire = 300 # seconds
        threshold=3
      }
      # Call an external command to blacklist a host
      blacklist {
        command = exec("/usr/local/bin/my_blacklist_command $ip")
      }
    }

    rules {
      # Definition of constants to reuse in patterns.
      % {
        # %{ip}: an IPv4 or IPv6 address
        ip = "(?P<ip>[0-9a-z-][0-9a-z-]*(?:[.:][0-9a-z-]*)+)"
      }

      # Facilities
      AUTH [
        ignore ".*: session opened"
        login_failure "sshd\\[[0-9]*\\]: Failed password for illegal user (?P<user>\\w*) from %{ip} "
      ]

      DAEMON [
        ignore "ntpd\\[[0-9]*\\]: kernel time sync status change"
        blacklist "named\\[[0-9]*\\]: client %{ip}#.* query \\(cache\\) .* denied"
      ]
    }

Custom actions can be defined in plugin files and loaded with --plugin-file

    def blacklist_host(ip):
        """Blacklist host with IP."""
        …

Used in rule

    actions {
      blacklist {
        command = blacklist_host("$ip")
      }
      …

