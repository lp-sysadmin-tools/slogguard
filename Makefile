# Copyright (C) 2011-2017 Laurent Pelecq <lpelecq+slogguard@circoise.eu>

# This file is part of Slogguard.
#
# Slogguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

COVERAGE_RUN = env PYTHONPATH=$(PWD) coverage run --source=slogguard
COVERAGE_REPORT = coverage report -m

build-dist:
	python setup.py bdist

test:
	env PYTHONPATH=$(PWD) python -m unittest discover

test-%:
	env PYTHONPATH=$(PWD) python tests/test_$*.py

coverage:
	$(COVERAGE_RUN) -m unittest discover
	$(COVERAGE_REPORT)

coverage-%:
	$(COVERAGE_RUN) tests/test_$*.py
	$(COVERAGE_REPORT)

debug-%:
	env PYTHONPATH=$(PWD) python -m pdb tests/test_$*.py

trace-%:
	env PYTHONPATH=$(PWD) python -X tracemalloc=10 tests/test_$*.py

clean:
	-rm -rf slogguard.egg-info build dist
	-rm -rf */__pycache__

install:
	python setup.py install --prefix $(INSTALL_DIR)

release:
	git diff --quiet || { echo "Error: there are uncommited changes"; exit 1; }
	VERSION=$$(sed "s/^__version__ = '\\(.*\\)'/\\1/" slogguard/version.py) &&	\
		git tag -a -m "Release $$VERSION" r$$VERSION &&					\
		git push --follow-tags
